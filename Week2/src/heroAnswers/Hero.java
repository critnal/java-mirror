package heroAnswers;

public interface Hero {
	
	String currentIdentity();
	void switchIdentity();
}
