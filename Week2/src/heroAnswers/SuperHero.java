package heroAnswers;

import heroQuestions.SuperPower;

public abstract class SuperHero implements Hero {
	
	protected String trueIdentity;
	protected String alterEgo;
	protected String currentIdentity;
	
	public SuperHero (String trueIdentity, String alterEgo) {
		this.trueIdentity = trueIdentity;
		this.alterEgo = alterEgo;
		currentIdentity = trueIdentity;
	}

	@Override
	public String currentIdentity() {
		// TODO Auto-generated method stub
		return currentIdentity;
	}

	@Override
	public void switchIdentity() {
		// TODO Auto-generated method stub
		currentIdentity = alterEgo;

	}
	
	public abstract boolean hasPower(SuperPower queriedPower);
	
	public abstract int totalPower();

}
