commit d3205ccc370a2aaa38d1b0c3d0bea681206c2aa7
Merge: 2ebda73 071f3fc
Author: Alexander Crichton <alexjcrichton01@gmail.com>
Date:   Tue May 28 21:55:47 2013 +1000

    @author Alexander Crichton

commit 2ebda737c375d4a9d1b59ceb47adcfd498ac53cd
Merge: f18798d 472ac76
Author: Kerry <XxKezza05xX@gmail.com>
Date:   Tue May 28 17:24:11 2013 +1000

    @author Alexander Crichton n6878296

commit 071f3fc230e642c36738492f453ea39e6386cf2a
Author: Kerry <XxKezza05xX@gmail.com>
Date:   Tue May 28 20:59:29 2013 +1000

    Kerry England
    
    OurTests.doc and Statement.txt added

commit 97b46d9579b19fcd0b39e424d798de5e7bc07ad1
Merge: f18798d 472ac76
Author: Kerry <XxKezza05xX@gmail.com>
Date:   Tue May 28 17:24:11 2013 +1000

    Merge branch 'master' of bitbucket.org:critnal/inb370

commit f18798d6a1c0e2e0e481efe16617e637cbe09bde
Author: Kerry <XxKezza05xX@gmail.com>
Date:   Tue May 28 17:23:52 2013 +1000

    Kerry England & Alex
    
    Finished Code

commit 472ac76ea7f73289eb272c2eb7037d6da8989d4e
Merge: c909571 cb0f86e
Author: Alexander Crichton <alexjcrichton01@gmail.com>
Date:   Tue May 28 02:02:54 2013 +1000

    Merge branch 'master' of bitbucket.org:critnal/inb370
    
    Conflicts:
    	bin/asgn2GUI/Size.class
    	bin/asgn2GUI/TrainGUI.class
    	src/asgn2GUI/Size.java
    	src/asgn2GUI/TrainGUI.java

commit c9095713eded8bfa095a362bac54401ce282829c
Author: Alexander Crichton <alexjcrichton01@gmail.com>
Date:   Mon May 27 18:35:38 2013 +1000

    @author Alexander Crichton n6878296 GUI work. Nearly finished Status panel.

commit cb0f86e8a65f7745042fb6edc9417b8744f6db33
Merge: 7e7ca9d df0a305
Author: Kerry <XxKezza05xX@gmail.com>
Date:   Mon May 27 22:33:40 2013 +1000

    Merge branch 'master' of bitbucket.org:critnal/inb370

commit df0a305659c5e8b166f7fdc1c26293970bf1b01b
Author: Alexander Crichton <alexjcrichton01@gmail.com>
Date:   Mon May 27 18:35:38 2013 +1000

    @author Alexander Crichton n6878296 GUI work. Size helper class.

commit 7e7ca9dbb95c37c92cae50b5754cac66fe810242
Merge: 72a033a 321e6d8
Author: Kerry <XxKezza05xX@gmail.com>
Date:   Mon May 27 13:37:04 2013 +1000

    Merge branch 'master' of bitbucket.org:critnal/inb370
    
    Conflicts:
    	bin/asgn2Exceptions/TrainException.class
    	bin/asgn2Tests/RollingStockTests.class

commit 72a033a350f6a6e3d678b22a80e54cc1e907e0fa
Author: Kerry <XxKezza05xX@gmail.com>
Date:   Mon May 27 13:32:34 2013 +1000

    Kerry England
    
    Commiting RollingStockTests.class

commit 46836eb7d409ac495f79faf9eb74cd3a743b1bdf
Author: Kerry <XxKezza05xX@gmail.com>
Date:   Mon May 27 13:24:39 2013 +1000

    Kerry England
    
    Commit. Not sure if it will break train exception

commit 321e6d80df1aa1e4596532d03f112cf058db93be
Author: Alexander Crichton <alexjcrichton01@gmail.com>
Date:   Sun May 26 23:46:40 2013 +1000

    @author Alexander Crichton Introduced the GUI classes.

commit f08622aad647e2a285d4f792e9636286f12cb23c
Merge: 49e6384 f23eae0
Author: Alexander Crichton <alexjcrichton01@gmail.com>
Date:   Sun May 26 18:39:15 2013 +1000

    @author Alexander Crichton
    Merge branch 'master' of bitbucket.org:critnal/inb370
    
    Conflicts:
    	bin/asgn2Tests/TrainTests.class
    	bin/asgn2Train/DepartingTrain.class
    	src/asgn2Train/DepartingTrain.java

commit 49e63846451eb514fc9750102f6cf5d5748ec919
Author: Alexander Crichton <alexjcrichton01@gmail.com>
Date:   Sun May 26 18:35:47 2013 +1000

    @author Alexander Crichton Added more tests for DepartingTrain.

commit f23eae02e3999dbac31c7112eece1e554a99e241
Author: Kerry <XxKezza05xX@gmail.com>
Date:   Sun May 26 18:33:58 2013 +1000

    Kerry England
    
    Added more tests, and fixed up DepartingTrain.java

commit c93f733f395c1ac195856c59ee291f73a11bb84f
Merge: f27a0fa a668df5
Author: Alexander Crichton <alexjcrichton01@gmail.com>
Date:   Sun May 26 14:56:13 2013 +1000

    Merge branch 'master' of bitbucket.org:critnal/inb370

commit f27a0faf5d6a499d79b7fdf984976897449dbd97
Merge: 29b5702 1d4bdc6
Author: Alexander Crichton <alexjcrichton01@gmail.com>
Date:   Sun May 26 13:43:09 2013 +1000

    @author Alexander Crichton n6878296 Added more tests for DepartingTrain.

commit a668df57220cd6b4974fe40a8b44390725769e36
Merge: 29b5702 1d4bdc6
Author: Alexander Crichton <alexjcrichton01@gmail.com>
Date:   Sun May 26 13:43:09 2013 +1000

    @author Alexander Crichton n6878296
    Merge branch 'master' of bitbucket.org:critnal/inb370
    
    Conflicts:
    	src/asgn2Tests/RollingStockTests.java
    	src/asgn2Train/DepartingTrain.java

commit 29b5702f2abd8e0bad03fd0134ca766bf4b8b858
Author: Alexander Crichton <alexjcrichton01@gmail.com>
Date:   Sun May 26 13:41:08 2013 +1000

    @author Alexander Crichton n6878296
    Added tests for DepartingTrain.

commit 1d4bdc6567c41389a71b0dfb58f3603b485d1f15
Author: Kerry <XxKezza05xX@gmail.com>
Date:   Sun May 26 13:35:22 2013 +1000

    @Author Kerry England
    
    Added my tests, and partially completed DepartingTrain.java class.

commit d0593330dede0884b81cc73a64d46761837cacd0
Author: Alexander Crichton <alexjcrichton01@gmail.com>
Date:   Sat May 25 17:49:27 2013 +1000

    @author Alexander Crichton n6878296
    
    Added the rest of the packages and classes.

commit 84d0c4169a90285905f0f97cedaa2d4b15c43e94
Author: Alexander Crichton <alexjcrichton01@gmail.com>
Date:   Sat May 25 16:34:27 2013 +1000

    Added ignore file

commit d175e9c7f519d49199c4ad8da3a7ae2a91d5284f
Author: Alexander Crichton <alexjcrichton01@gmail.com>
Date:   Sat May 25 16:30:09 2013 +1000

    Made the project
    
    Signed-off-by: Alexander Crichton <alexjcrichton01@gmail.com>
