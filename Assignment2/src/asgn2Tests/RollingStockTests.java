package asgn2Tests;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn2Exceptions.TrainException;
import asgn2RollingStock.FreightCar;
import asgn2RollingStock.Locomotive;
import asgn2RollingStock.PassengerCar;
import asgn2RollingStock.RollingStock;

/**
 * Test cases for the RollingStock package to ensure that classes work how they
 * are intended to work, as well as pick up errors which may occur.
 * 
 * @author Kerry England
 *
 */

public class RollingStockTests {
	
	RollingStock testCarriage;
	FreightCar testFreightCar;
	Locomotive testLocomotive;
	PassengerCar testPassengerCar;
	Integer testWeight = 5;
	String testGoodsType = "D";
	Integer testSeats = 10;
	Integer testPassengers = 5;
	Integer testRejectedPassengers = 0;
	String testLocomotiveType = "1E";

	/**
	 * Resets Variables before each test case
	 * 
	 * @throws Exception
	 */
	@Before
	public void before() throws Exception {
		testWeight = 5;
		testGoodsType = "D";
		testSeats = 10;
		testPassengers = 5;
		testLocomotiveType = "1E";
	}

	/**
	 * Tests the construction of a freight car
	 * 
	 * @throws TrainException
	 */
	@Test
	public void testfreightCar() throws TrainException {
		testFreightCar = new FreightCar(testWeight, testGoodsType);
		assertEquals(testFreightCar.goodsType(), testGoodsType);
		assertEquals(testFreightCar.getGrossWeight(), testWeight);
	}
	/**
	 * Tests if the freight car has invalid cargo
	 * 
	 * @throws TrainException
	 */
	@Test (expected = TrainException.class)
	public void testInvalidFreightCarType() throws TrainException {
		testGoodsType = "Q";
		testFreightCar = new FreightCar(testWeight, testGoodsType);
	}
	
	/**
	 * Tests if the freight car has negative weight
	 * 
	 * @throws TrainException
	 */
	@Test (expected = TrainException.class)
	public void testInvalidFreightCarWeight() throws TrainException {
		testWeight = -3;
		testFreightCar = new FreightCar(testWeight, testGoodsType);
	}
	
	/**
	 * Tests if the freight car has no weight
	 * 
	 * @throws TrainException
	 */
	@Test (expected = TrainException.class)
	public void testZeroFreightCarWeight() throws TrainException {
		testWeight = 0;
		testFreightCar = new FreightCar(testWeight, testGoodsType);
	}
	
	/**
	 * Tests the construction of a locomotive
	 * 
	 * @throws TrainException
	 */
	@Test
	public void testLocomotive() throws TrainException {
		testLocomotive = new Locomotive(testWeight, testLocomotiveType);
		assertEquals(testLocomotive.getGrossWeight(), testWeight);
	}
	
	/**
	 * Tests if the locomotive has an invalid classification code, but with a valid form
	 * 
	 * @throws TrainException
	 */
	@Test (expected = TrainException.class)
	public void testInvalidLocomotiveCode() throws TrainException {
		testLocomotiveType = "1Z";
		testLocomotive = new Locomotive(testWeight, testLocomotiveType);
	}
	
	/**
	 * Tests if the locomotive classification is overly invalid
	 * 
	 * @throws TrainException
	 */
	@Test (expected = TrainException.class)
	public void testLongLocomotiveCode() throws TrainException {
		testLocomotiveType = "2Ayz&ABee";
		testLocomotive = new Locomotive(testWeight, testLocomotiveType);
	}
	
	/**
	 * Tests if the locomotive has negative weight
	 * 
	 * @throws TrainException
	 */
	@Test (expected = TrainException.class)
	public void testInvalidLocomotiveWeight() throws TrainException {
		testWeight = -3;
		testLocomotive = new Locomotive(testWeight, testLocomotiveType);
	}
	
	/**
	 * Tests if the locomotive has no weight
	 * 
	 * @throws TrainException
	 */
	@Test (expected = TrainException.class)
	public void testZeroWeightLocomotive() throws TrainException {
		testWeight = 0;
		testLocomotive = new Locomotive(testWeight, testLocomotiveType);
		assertEquals(testLocomotive.getGrossWeight(), testWeight);
	}
	
	/**
	 * Tests the construction of a passenger car
	 * 
	 * @throws TrainException
	 */
	@Test
	public void testPassengerCar() throws TrainException {
		testPassengerCar = new PassengerCar(testWeight, testSeats);
		assertEquals(testPassengerCar.numberOfSeats(), testSeats);
		assertEquals(testPassengerCar.getGrossWeight(), testWeight);
	}
	
	/**
	 * Tests if the passenger car has zero seats
	 * 
	 * @throws TrainException
	 */
	@Test
	public void testZeroPassengerCarSeats() throws TrainException {
		testSeats = 0;
		testPassengerCar = new PassengerCar(testWeight, testSeats);
		assertEquals(testPassengerCar.numberOfSeats(), testSeats);
	}
	
	/**
	 * Tests passengers boarding the train
	 * 
	 * @throws TrainException
	 */
	public void testPassengersBoarding() throws TrainException {
		testPassengerCar = new PassengerCar(testWeight, testSeats);
		testPassengerCar.board(testPassengers);
		assertEquals(testPassengerCar.numberOnBoard(), testPassengers);
	}
	
	/**
	 * Tests zero passengers boarding
	 * 
	 * @throws TrainException
	 */
	public void testZeroPassengersBoarding() throws TrainException {
		testPassengers = 0;
		testPassengerCar = new PassengerCar(testWeight, testSeats);
		testPassengerCar.board(testPassengers);
		assertEquals(testPassengerCar.numberOnBoard(), testPassengers);
		}
	
	/**
	 * Tests if the number of boarding passengers exceeds the amount of seats
	 * 
	 * @throws TrainException
	 */
	@Test
	public void testTooManyPassengers() throws TrainException {
		testPassengers = 15;
		testPassengerCar = new PassengerCar(testWeight, testSeats);
		testRejectedPassengers = testPassengerCar.board(testPassengers);
		assertTrue(5 ==  testRejectedPassengers);
//		assertThat(testPassengerCar, instanceOf(PassengerCar.class));
//		assertEquals(testPassengerCar.numberOnBoard(), testSeats);
//		assertTrue(testRejectedPassengers == (testPassengers - testSeats));
	}
	
	/** 
	 * Tests if the passenger car has negative seats
	 * 
	 * @throws TrainException
	 */
	@Test (expected = TrainException.class)
	public void testInvalidPassengerCarSeats() throws TrainException {
		testSeats = -4;
		testPassengerCar = new PassengerCar(testWeight, testSeats);
		
	}
	
	/** 
	 * Tests if the passenger car has negative weight
	 * 
	 * @throws TrainException
	 */
	@Test (expected = TrainException.class)
	public void testInvalidPassengerCarWeight() throws TrainException {
		testWeight = -3;
		testPassengerCar = new PassengerCar(testWeight, testSeats);
	}
	
	/** 
	 * Tests if the passenger car is boarding negative passengers
	 * 
	 * @throws TrainException
	 */
	@Test (expected = TrainException.class)
	public void testInvalidPassengersBoarding() throws TrainException {
		testPassengers = - 4;
		testPassengerCar = new PassengerCar(testWeight, testSeats);
		testPassengerCar.board(testPassengers);
	}
	
	/**
	 * Tests if the passenger car has zero weight
	 * 
	 * @throws TrainException
	 */
	@Test (expected = TrainException.class)
	public void testZeroWeightPassengerCar() throws TrainException{
		testWeight = 0;
		testPassengerCar = new PassengerCar(testWeight, testSeats);
		assertEquals(testPassengerCar.getGrossWeight(), testWeight);
	}

}
