package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.instanceOf;

import asgn2Exceptions.TrainException;
import asgn2RollingStock.FreightCar;
import asgn2RollingStock.Locomotive;
import asgn2RollingStock.PassengerCar;
import asgn2Train.DepartingTrain;

public class TrainTests {

	DepartingTrain train;

	@Before
	public void before() {
		train = new DepartingTrain();
		assertNull(train.firstCarriage());
	}
	
	@Test
	public void testNull() throws TrainException {
		assertNull(train.firstCarriage());
	}

	/**
	 * Test that a carriage can be added.
	 * 
	 * @throws TrainException
	 *             If firstCarriage() doesn't return a carriage after adding a
	 *             carriage.
	 */
	@Test
	public void testAddCarriage() throws TrainException {
		Locomotive locomotive = new Locomotive(1, "1E");
		train.addCarriage(locomotive);
		assertThat(train.firstCarriage(), instanceOf(Locomotive.class));
	}
	
	@Test(expected = TrainException.class)
	public void testRemoveCarriageEmptyTrain() throws TrainException {
		train.removeCarriage();
	}

	/**
	 * Test that a carriage can be removed.
	 * 
	 * @throws TrainException
	 *             If firstCarriage() returns a carriage after adding and
	 *             removing a single carriage.
	 */
	@Test
	public void testRemoveCarriage() throws TrainException {
		Locomotive locomotive = new Locomotive(1, "1E");
		train.addCarriage(locomotive);
		assertThat(train.firstCarriage(), instanceOf(Locomotive.class));
		train.removeCarriage();
		assertNull(train.firstCarriage());
	}

	/**
	 * Test that locomotives can only be added when there are no other
	 * carriages.
	 * 
	 * @throws TrainException
	 *             Expected to throw TrainException when adding a locomotive
	 *             after adding a first carriage.
	 */
	@Test(expected = TrainException.class)
	public void testCanOnlyHaveOneLocomotive() throws TrainException {
		Locomotive locomotive = new Locomotive(1, "1E");
		train.addCarriage(locomotive);
		train.addCarriage(locomotive);
	}

	/**
	 * Test that freight cars can't be added to the front of the train.
	 * 
	 * @throws TrainException
	 *             When trying to add freight car to the front.
	 */
	@Test(expected = TrainException.class)
	public void testFreightCarsCannotBeAddedToFront() throws TrainException {
		FreightCar freightCar = new FreightCar(1, "G");
		train.addCarriage(freightCar);
	}

	/**
	 * Test that passenger cars can't be added to the front of the train.
	 * 
	 * @throws TrainException
	 *             When trying to add passenger car to the front.
	 */
	@Test(expected = TrainException.class)
	public void testPassengerCarsCannotBeAddedToFront() throws TrainException {
		PassengerCar passengerCar = new PassengerCar(1, 1);
		train.addCarriage(passengerCar);
	}

	/**
	 * Test that passenger cars can be added before freight cars, and that
	 * freight cars can be added after passenger cars.
	 * 
	 * @throws TrainException
	 *             If passenger car can't be added before freight car, or
	 *             freight car can't be added after passenger car.
	 */
	@Test
	public void testPassengerCarThenFreightCar() throws TrainException {
		Locomotive locomotive = new Locomotive(1, "1E");
		train.addCarriage(locomotive);
		PassengerCar passengerCar = new PassengerCar(1, 1);
		train.addCarriage(passengerCar);
		FreightCar freightCar = new FreightCar(1, "G");
		train.addCarriage(freightCar);
		assertThat(train.firstCarriage(), instanceOf(Locomotive.class));
		assertThat(train.nextCarriage(), instanceOf(PassengerCar.class));
		assertThat(train.nextCarriage(), instanceOf(FreightCar.class));
	}

	/**
	 * Test that passenger cars can only be added before freight cars.
	 * 
	 * @throws TrainException
	 *             When trying to add passenger car after freight car.
	 */
	@Test(expected = TrainException.class)
	public void testPassengerCarsAfterFreightCar() throws TrainException {
		Locomotive locomotive = new Locomotive(1, "1E");
		train.addCarriage(locomotive);
		FreightCar freightCar = new FreightCar(1, "G");
		train.addCarriage(freightCar);
		PassengerCar passengerCar = new PassengerCar(1, 1);
		train.addCarriage(passengerCar);
	}

	/**
	 * Test that nextCarriage() doesn't try to return anything when called on an
	 * empty train.
	 * 
	 * @throws TrainException
	 */
	@Test
	public void testNextCarriageDoesntReturnOnEmptyTrain()
			throws TrainException {
		assertNull(train.nextCarriage());
	}

	/**
	 * Test that nextCarriage() doesn't try to return anything when called on
	 * the last carriage.
	 * 
	 * @throws TrainException
	 */
	@Test
	public void testNextCarriageDoesntReturnAfterLastCarriage()
			throws TrainException {
		Locomotive locomotive = new Locomotive(1, "1E");
		train.addCarriage(locomotive);
		train.firstCarriage();
		assertNull(train.nextCarriage());
	}

	/**
	 * Test that trainCanMove() can return true;
	 * 
	 * @throws TrainException
	 */
	@Test
	public void testThatTrainCanMove() throws TrainException {
		Locomotive locomotive = new Locomotive(1, "1E");
		train.addCarriage(locomotive);
		assertTrue(train.trainCanMove());
	}

	/**
	 * Test that trainCanMove() can return false;
	 * 
	 * @throws TrainException
	 */
	@Test
	public void testThatTrainCanNotMove() throws TrainException {
		Locomotive locomotive = new Locomotive(999999, "1E");
		train.addCarriage(locomotive);
		assertFalse(train.trainCanMove());
	}

	/**
	 * Test that numberOfSeats can return the number of seats.
	 * 
	 * @throws TrainException
	 */
	@Test
	public void testNumberOfSeats() throws TrainException {
		Locomotive locomotive = new Locomotive(1, "1E");
		train.addCarriage(locomotive);
		PassengerCar passengerCar = new PassengerCar(1, 1);
		train.addCarriage(passengerCar);
		assertEquals((Integer) 1, train.numberOfSeats());
	}

	/**
	 * Test that numberOfSeats can return the correct number of seats.
	 * 
	 * @throws TrainException
	 */
	@Test
	public void testNumberOfSeatsCorrect() throws TrainException {
		Locomotive locomotive = new Locomotive(1, "1E");
		train.addCarriage(locomotive);
		PassengerCar passengerCar = new PassengerCar(1, 1);
		train.addCarriage(passengerCar);
		train.addCarriage(passengerCar);
		assertEquals((Integer) 2, train.numberOfSeats());
	}

	/**
	 * Test that passengers can board the train, and that none are incorrectly
	 * left over.
	 * 
	 * @throws TrainException
	 */
	@Test
	public void testBoardWorks() throws TrainException {
		Locomotive locomotive = new Locomotive(1, "1E");
		train.addCarriage(locomotive);
		PassengerCar passengerCar = new PassengerCar(1, 1);
		train.addCarriage(passengerCar);
		assertEquals((Integer) 0, train.board(1));
	}

	/**
	 * Test that passengers can board the train, and that the correct number are
	 * left over.
	 * 
	 * @throws TrainException
	 */
	@Test
	public void testBoardCorrectNumberLeftOver() throws TrainException {
		Locomotive locomotive = new Locomotive(1, "1E");
		train.addCarriage(locomotive);
		PassengerCar passengerCar = new PassengerCar(1, 1);
		train.addCarriage(passengerCar);
		assertEquals((Integer) 1, train.board(2));
	}
	
	
}
