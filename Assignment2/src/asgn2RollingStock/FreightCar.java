/**
 * @author	Alexander Crichton n6878296
 * @version	1.0
 * @since	23/05/2013
 */
package asgn2RollingStock;

import asgn2Exceptions.TrainException;

public class FreightCar extends RollingStock {

	private final String goodsType;

	/**
	 * Constructs a freight car object.
	 * 
	 * @param grossWeight
	 *            The freight car's gross weight in tonnes.
	 * @param goodsType
	 *            The type of goods the car is designed to carry.
	 * @throws TrainException
	 *             If the gross weight is not positive or if the good's type is
	 *             invalid.
	 */
	public FreightCar(Integer grossWeight, String goodsType) throws TrainException {
		super(grossWeight);
		if (goodsType.length() != 1) {
			throw new TrainException("Goods type must be one character");
		} else if ((goodsType == "G") || (goodsType == "R")
				|| (goodsType == "D")) {
			this.goodsType = goodsType;
		} else {
			throw new TrainException("Invalid goods type");
		}
	}

	/**
	 * Returns the type of goods this carriage was designed to carry.
	 * 
	 * @return The goodsType ("G", "R", or "D").
	 */
	public String goodsType() {
		return goodsType;

	}

	/**
	 * Returns a human-readable description the freight car. It has the form
	 * "Freight(x)" where x is a character indicating the type of goods the car
	 * is designed to carry.
	 * 
	 * @return A human-readable description of the freight car.
	 */
	@Override
	public String toString() {
		return ("Freight(" + goodsType + ")");
	}

}
