/**
 * @author	Alexander Crichton n6878296
 * @version	1.0
 * @since	23/05/2013
 */
package asgn2RollingStock;

import asgn2Exceptions.TrainException;

public class Locomotive extends RollingStock {

	private final String classification;

	/**
	 * Constructs a new locomotive object
	 * 
	 * @param grossWeight
	 *            The locomotive's weight in tonnes.
	 * @param classification
	 *            The locomitive's two-character classification code.
	 * @throws TrainException
	 *             If the locomotive weight is not strictly positive or if its
	 *             classification code is invalid.
	 */
	public Locomotive(Integer grossWeight, String classification)
			throws TrainException {
		super(grossWeight);
		if (classification.length() != 2) {
			throw new TrainException("Incorrect classification code length");
		} 
		else if (!(Character.getNumericValue(classification.charAt(0)) >= Character.getNumericValue('0'))
				|| !(Character.getNumericValue(classification.charAt(0)) <= Character.getNumericValue('9')))
		{
			throw new TrainException(
					"First character of classification is not a digit");
		} 
		else if ( !((Character.compare(classification.charAt(1), 'E') == 0))
				&& !((Character.compare(classification.charAt(1), 'D') == 0))
				&& !((Character.compare(classification.charAt(1), 'S') == 0)) ) {
			throw new TrainException(
					"Second character of classification is not an engine type");
			
		}
		else {
			this.classification = classification;
		}
	}

	/**
	 * Returns how much total weight the locomotive can pull (including itself).
	 * 
	 * @return The locomotive's pulling power in tonnes.
	 */
	public Integer power() {
		Integer power = new Integer(classification.charAt(0));
		return ((power * 100) - grossWeight);
	}

	/**
	 * Returns a human-readable description the locomotive. It has the form
	 * "Loco(x)" where x is the locomitive's two character classification code.
	 * 
	 * @return A human-readable description of the locomotive.
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return ("Loco(" + classification + ")");
	}

}
