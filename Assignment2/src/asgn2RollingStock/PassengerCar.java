/**
 * @author	Alexander Crichton n6878296
 * @version	1.0
 * @since	23/05/2013
 */
package asgn2RollingStock;

import asgn2Exceptions.TrainException;

public class PassengerCar extends RollingStock {

	private final Integer numberOfSeats;
	private Integer numberOnBoard = 0;

	public PassengerCar(Integer grossWeight, Integer numberOfSeats)
			throws TrainException {
		super(grossWeight);
		if (numberOfSeats >= 0) {
			this.numberOfSeats = numberOfSeats;
		}
		else {
			throw new TrainException("Cannot have a negative number of seats");
		}
	}

	/**
	 * Adds the given number of new passengers to the number on board the
	 * carriage.
	 * 
	 * @param newPassengers
	 *            The number of passengers to add.
	 * @return The number of passengers who were unable to board the carriage
	 *         because they couldn't get a seat.
	 * @throws TrainException
	 *             If the number of new passengers is negative.
	 */
	public Integer board(Integer newPassengers) throws TrainException {
		if (newPassengers < 0) {
			throw new TrainException(
					"Trying to add negative number of passengers");
		}
		numberOnBoard += newPassengers;
		Integer numberRejected = 0;
		if (numberOnBoard > numberOfSeats) {
			numberRejected = numberOnBoard - numberOfSeats;
			numberOnBoard = numberOfSeats;
		}
		return numberRejected;
	}

	/**
	 * Removes the given number of passengers from the number on board the
	 * carriage.
	 * 
	 * @param departingPassengers
	 *            The number of passengers to remove.
	 * @throws TrainException
	 *             If the number of departing passengers is negative or if the
	 *             number of departing passengers exceeds the number on board.
	 */
	public void alight(Integer departingPassengers) throws TrainException {
		if (departingPassengers < 0) {
			throw new TrainException(
					"Trying to remove negative number of passengers");
		}
		if (numberOnBoard - departingPassengers < 0) {
			throw new TrainException(
					"Trying to remove more passengers than are on board");
		} else {
			numberOnBoard -= departingPassengers;
		}
	}

	/**
	 * Returns the number of passengers currently on board this carriage.
	 * 
	 * @return The number of passengers on board.
	 */
	public Integer numberOnBoard() {
		return numberOnBoard;
	}

	/**
	 * Returns the number of seats installed on this carriage.
	 * 
	 * @return The number of seats on this carriage.
	 */
	public Integer numberOfSeats() {
		return numberOfSeats;
	}

	/**
	 * Returns a human-readable description the passenger car. It has the form
	 * "Passenger(x/y)" where x is the number of passengers currently on board
	 * and y is the number of seats in the carriage.
	 * 
	 * @return A human-readable description of the passenger car.
	 */
	@Override
	public String toString() {
		return ("Passengers(" + numberOnBoard + "/" + numberOfSeats + ")");
	}

}
