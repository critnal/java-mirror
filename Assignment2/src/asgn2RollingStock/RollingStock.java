/**
 * @author	Alexander Crichton n6878296
 * @version	1.0
 * @since	23/05/2013
 */
package asgn2RollingStock;

import asgn2Exceptions.TrainException;

public abstract class RollingStock {

	protected final Integer grossWeight;

	/**
	 * Constructs a railway carriage with a specific gross weight.
	 * 
	 * @param grossWeight
	 *            The carriage's gross weight in tonnes.
	 * @throws TrainException
	 *             If the gross weight is not positive.
	 */
	public RollingStock(Integer grossWeight) throws TrainException {
		if (grossWeight <= 0) {
			throw new TrainException(
					"Trying to assign a negative gross weight.");
		} else {
			this.grossWeight = grossWeight;
		}
	}

	/**
	 * Returns the railway carriage's gross weight.
	 * 
	 * @return The carriage's gross weight in tonnes.
	 */
	public Integer getGrossWeight() {
		return grossWeight;
	}

	@Override
	public abstract String toString();
}
