package asgn2Train;

import java.util.ArrayList;

import asgn2Exceptions.TrainException;
import asgn2RollingStock.FreightCar;
import asgn2RollingStock.Locomotive;
import asgn2RollingStock.PassengerCar;
import asgn2RollingStock.RollingStock;

/**
 * A sequence of carriages. Contains various operations for the departure of
 * the train, as well as checking to see if the train is ready for departure
 * 
 * @author Kerry England
 *
 */
public class DepartingTrain {
	
	private ArrayList<RollingStock> train = new ArrayList<RollingStock>();
	private Integer nextIndex = 0;
	private Integer passengersOn = 0;
	Integer rejectedPassengers = 0;
	
	/**
	 * Adds a new carriage to the train
	 * 
	 * @param newCarriage
	 * 				The carriage to be added
	 * @throws TrainException
	 * 				Exception thrown when an invalid carriage is added
	 */
	public void addCarriage(RollingStock newCarriage) throws TrainException {
			if (train.size() == 0) {
				if (newCarriage instanceof Locomotive) {
					train.add(newCarriage);
				}
				else {
					throw new TrainException("Can only have a locomotive first");
				}
			}
			else if (train.size() > 0) {
				if (newCarriage instanceof Locomotive) {
					throw new TrainException("Can only have one locomotive");
				}
				else if (newCarriage instanceof PassengerCar && !(train.get(train.size() - 1) instanceof FreightCar)) {
					train.add(newCarriage);
				}
				else if (newCarriage instanceof FreightCar) {
					train.add(newCarriage);
				}
				else {
					throw new TrainException("Cannot put a passenger car after a freight car");
				}
			}
	}
	
	/**
	 * Adds passengers onto the train in any passenger carriage
	 * 
	 * @param newPassengers
	 * 				The number of passengers to board the train
	 * @return
	 * 				The total number of passengers who can't fit on the train
	 * @throws TrainException
	 * 				Exception thrown when number of passengers to add is negative
	 */
	public Integer board(Integer newPassengers) throws TrainException {	
		rejectedPassengers = newPassengers;
		if (newPassengers >= 0) {
			for (Integer i = 0; i < train.size(); i++) {
				if (train.get(i) instanceof PassengerCar) {
					rejectedPassengers = ((PassengerCar)train.get(i)).board(rejectedPassengers);
				}
			}
		}
		else {
			throw new TrainException("Cannot load negative number of passengers");
		}
		return rejectedPassengers;
	}
	
	/**
	 * Gets the value of the first carriage
	 * 
	 * @return
	 * 			The first carriage of the train
	 */
	public RollingStock firstCarriage() {
		if (train.isEmpty()) {
			return null;
		}
		else {
			nextIndex = 0;
			return train.get(0);
		}
	}
	
	/**
	 * Gets the value of the next carriage on the train.
	 * 
	 * @return
	 * 			The next carriage along the train from the last seen carriage
	 */
	public RollingStock nextCarriage() {
		if (train.isEmpty()){
			return null;
		}
		else if (nextIndex == train.size() - 1) {
			return null;
		}
		else {
			nextIndex ++;
			return train.get(nextIndex);
		}
	}
	
	/**
	 * Sets the total number of seats on the train
	 * 
	 * @return
	 * 			The total number of seats on the train
	 */
	public Integer numberOfSeats() {
		Integer totalSeats = 0;
		for (int i = 0; i < train.size(); i++) {
			if (train.get(i) instanceof PassengerCar) {
				totalSeats += ((PassengerCar)train.get(i)).numberOfSeats();
			}
		}
		return totalSeats;
	}
	
	/**
	 * Gets the total number of passengers on board
	 * 
	 * @return
	 * 			The total number of passengers on board
	 */
	public Integer numberOnBoard() {
		return passengersOn;
	}
	
	/**
	 * Removes the last carriage from the train
	 * 
	 * @throws TrainException
	 * 			Thrown when there is passengers on board
	 */
	public void removeCarriage() throws TrainException{
		if (train.isEmpty()){
			throw new TrainException("No carriages to remove");
		}
		if (passengersOn <= 0) {
			System.out.println(train.size());
			train.remove(train.size() - 1);
			System.out.println(train.size());
		}
		else {
			throw new TrainException("Cannot shunt while passengers are on board");
		}
	}
	
	/**
	 * Returns a human-readable string to display the train
	 */
	@Override
	public String toString() {
		String printString = "";
		for (int i = 0; i < train.size(); i++){
			printString += (" " + train.get(i).toString());
			}
		return printString;
	}
	
	/**
	 * Gets whether the train is capable of moving
	 * 
	 * @return
	 * 			Whether the train can move (true) or not (false)
	 */
	public boolean trainCanMove() {
		Integer weight = 0;
		Integer power = 0;
		if (!train.isEmpty()) {
			power = ((Locomotive)train.get(0)).power();
			for (int i = 0; i < train.size(); i++){
				weight += train.get(i).getGrossWeight();
			}
			if (weight <= power) {
				return true;
			}
			else { 
				return false;
			}
		}
		else {
			return false;
		}
	}
}
