package asgn2Exceptions;

@SuppressWarnings("serial")
public class TrainException extends Exception {
	
	/**
	 * Creates a new instance of TrainException.
	 * 
	 * @param msg An informative message about the problem found.
	 */
	public TrainException(String msg) {
		super("Train Exception: " + msg);
	}

}
