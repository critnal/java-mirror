/**
 * @author	Alexander Crichton n6878296
 * @author Kerry England 
 * @version	1.0
 * @since	23/05/2013
 */
package asgn2GUI;

import java.awt.Dimension;

public final class Size {
	
	public static final Dimension MAX = new Dimension(9999, 9999);
	
	public static final Dimension INPUT_PANEL = new Dimension(400, 600);
	public static final Dimension DISPLAY_PANEL = new Dimension(600, 600);
	public static final Dimension STATUS_PANEL = new Dimension(100, 200);
	public static final Dimension CARRIAGES_PANEL = new Dimension(100, 100);
	
	public static final Dimension INPUT_PANEL_PANEL = new Dimension(400, 80);
	public static final Dimension INPUT_PANEL_PANEL_BUTTON = new Dimension(400, 40);
	public static final Dimension INPUT_PANEL_PANEL_FIELD = new Dimension(400, 40);
	
	public static final Dimension LOAD_AND_SEATING_PANELS = new Dimension(250, 200);
	
	public static final Dimension LOAD_PANEL_TOP = new Dimension(250, 60);
	
	public static final Dimension CAR_PANEL = new Dimension(150, 300);
}
