/**
 * @author	Alexander Crichton n6878296
 * @author Kerry England 
 * @version	1.0
 * @since	23/05/2013
 */
package asgn2GUI;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsConfiguration;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;

public class TrainGUI extends JFrame {

	private static final long serialVersionUID = 1L;

	/*
	 * Input elements
	 */
	private JButton buttonAddLocomotive;
	private JButton buttonAddPassengerCar;
	private JButton buttonAddFreightCar;
	private JButton buttonRemoveCar;
	private JButton buttonRemoveAllCars;
	private JButton buttonAddPassengers;

	private JComboBox comboboxAddLocomotiveClassNumber;
	private JComboBox comboboxAddLocomotiveClassLetter;
	private JComboBox comboboxAddFreightCarType;

	private JSpinner spinnerAddLocomotiveWeight;
	private JSpinner spinnerAddPassengerCarWeight;
	private JSpinner spinnerAddPassengerCarCapacity;
	private JSpinner spinnerAddFreightCarWeight;
	private JSpinner spinnerAddPassengers;

	/*
	 * Status elements
	 */
	private JLabel labelLoadCapacity;
	private JLabel labelLoadCurrent;
	private JLabel labelLoadRemaining;
	private JLabel labelLoadCanMove;
	private JLabel labelSeatingCapacity;
	private JLabel labelSeatingCurrent;
	private JLabel labelSeatingRemaining;
	private JLabel labelPassengersLeftOver;
	private JLabel labelSeatingAvailable;
	
	/*
	 * Carriage elements
	 */
	private JPanel pnlDisplayStatus;
	private JPanel pnlDisplayCarriages;

	public TrainGUI() throws HeadlessException {

		/*
		 * TrainGUI has two top level panels. One for user input, the other to
		 * display train information.
		 */
		int totalWidth = Size.INPUT_PANEL.width + Size.DISPLAY_PANEL.width;
		int totalHeight = Size.INPUT_PANEL.height;
		setSize(totalWidth, totalHeight);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BoxLayout(this.getContentPane(), BoxLayout.X_AXIS));

		// Set up the input panel
		add(pnlInput());

		// Set up the display panel to hold the status and carriage displays
		add(pnlDisplay());
	}

	private JPanel pnlInput() {
		JPanel pnlInput = new JPanel();
		pnlInput.setLayout(new BoxLayout(pnlInput, BoxLayout.Y_AXIS));
		pnlInput.setMinimumSize(Size.INPUT_PANEL);
		pnlInput.setPreferredSize(Size.INPUT_PANEL);
		pnlInput.setMaximumSize(new Dimension(Size.INPUT_PANEL.width,
				Size.MAX.height));
		pnlInput.setBorder(BorderFactory.createLineBorder(Color.gray, 5));

		pnlInput.add(locomotivePanel());
		pnlInput.add(Box.createRigidArea(new Dimension(0, 20)));
		pnlInput.add(passengerCarPanel());
		pnlInput.add(Box.createRigidArea(new Dimension(0, 20)));
		pnlInput.add(freightCarPanel());
		pnlInput.add(Box.createRigidArea(new Dimension(0, 20)));
		pnlInput.add(removeCarPanel());
		pnlInput.add(Box.createRigidArea(new Dimension(0, 20)));
		pnlInput.add(passengersPanel());

		return pnlInput;
	}

	private JPanel pnlDisplay() {
		JPanel pnlDisplay = new JPanel();

		pnlDisplay = new JPanel();
		pnlDisplay.setLayout(new BoxLayout(pnlDisplay, BoxLayout.Y_AXIS));
		pnlDisplay.setMinimumSize(Size.DISPLAY_PANEL);
		pnlDisplay.setPreferredSize(Size.DISPLAY_PANEL);
		pnlDisplay.setBorder(BorderFactory.createLineBorder(Color.gray, 5));

		pnlDisplay.add(pnlDisplayStatus());
		pnlDisplay.add(pnlDisplayCarriages());

		return pnlDisplay;
	}

	private JPanel pnlDisplayStatus() {

		/*
		 * pnlDisplayStatus is the top right panel that shows all the load and
		 * seating information. It's divided into two sub-panels that display
		 * the load information and seating information.
		 */
		pnlDisplayStatus = new JPanel();

		pnlDisplayStatus.setAlignmentX(Component.LEFT_ALIGNMENT);
		pnlDisplayStatus.setLayout(new BoxLayout(pnlDisplayStatus,
				BoxLayout.X_AXIS));
		pnlDisplayStatus.setMinimumSize(Size.STATUS_PANEL);
		pnlDisplayStatus.setPreferredSize(Size.STATUS_PANEL);
		pnlDisplayStatus.setMaximumSize(new Dimension(Size.MAX.width,
				Size.STATUS_PANEL.height));
		pnlDisplayStatus.setBorder(BorderFactory
				.createLineBorder(Color.gray, 5));

		pnlDisplayStatus.add(loadPanel());
		pnlDisplayStatus.add(Box.createRigidArea(new Dimension(50, 0)));
		pnlDisplayStatus.add(seatingPanel());

		return pnlDisplayStatus;
	}

	private JPanel pnlDisplayCarriages() {
		pnlDisplayCarriages = new JPanel();

		pnlDisplayCarriages.setAlignmentX(Component.LEFT_ALIGNMENT);
		pnlDisplayCarriages.setMinimumSize(Size.CARRIAGES_PANEL);
		pnlDisplayCarriages.setPreferredSize(Size.CARRIAGES_PANEL);
		pnlDisplayCarriages.setBorder(BorderFactory.createLineBorder(
				Color.gray, 5));
		
		pnlDisplayCarriages.add(new JLabel("Test"));

		return pnlDisplayCarriages;
	}

	private JPanel locomotivePanel() {
		JPanel locomotivePanel = new JPanel();
		locomotivePanel.setLayout(new BoxLayout(locomotivePanel,
				BoxLayout.Y_AXIS));
		locomotivePanel.setMinimumSize(Size.INPUT_PANEL_PANEL);
		locomotivePanel.setPreferredSize(Size.INPUT_PANEL_PANEL);
		locomotivePanel.setMaximumSize(Size.INPUT_PANEL_PANEL);

		locomotivePanel.add(locomotivePanelButton());
		locomotivePanel.add(locomotivePanelFields());

		return locomotivePanel;
	}

	private JPanel locomotivePanelButton() {
		JPanel locomotivePanelButton = new JPanel();
		locomotivePanelButton.setMinimumSize(Size.INPUT_PANEL_PANEL_BUTTON);
		locomotivePanelButton.setPreferredSize(Size.INPUT_PANEL_PANEL_BUTTON);
		locomotivePanelButton.setMaximumSize(Size.INPUT_PANEL_PANEL_BUTTON);

		buttonAddLocomotive = newInputButton("Add Locomotive");
		locomotivePanelButton.add(buttonAddLocomotive);

		return locomotivePanelButton;
	}

	private JPanel locomotivePanelFields() {
		JPanel locomotivePanelFields = new JPanel();
		locomotivePanelFields.setLayout(new BoxLayout(locomotivePanelFields,
				BoxLayout.X_AXIS));
		locomotivePanelFields.setMaximumSize(Size.INPUT_PANEL_PANEL_FIELD);

		locomotivePanelFields.add(new JLabel("Weight: "));
		spinnerAddLocomotiveWeight = newInputSpinner(1, 0, 1000, 1);
		locomotivePanelFields.add(spinnerAddLocomotiveWeight);

		// Space between inputs
		locomotivePanelFields.add(Box.createRigidArea(new Dimension(10, 0)));

		locomotivePanelFields.add(new JLabel("Class: "));

		String[] numbers = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		comboboxAddLocomotiveClassNumber = new JComboBox(numbers);
		comboboxAddLocomotiveClassNumber.setSelectedIndex(0);
		locomotivePanelFields.add(comboboxAddLocomotiveClassNumber);

		String[] letters = { "D", "E", "S" };
		comboboxAddLocomotiveClassLetter = new JComboBox(letters);
		comboboxAddLocomotiveClassLetter.setSelectedIndex(0);
		locomotivePanelFields.add(comboboxAddLocomotiveClassLetter);

		return locomotivePanelFields;
	}

	private JPanel passengerCarPanel() {
		JPanel passengerCarPanel = new JPanel();

		passengerCarPanel.setLayout(new BoxLayout(passengerCarPanel,
				BoxLayout.Y_AXIS));
		passengerCarPanel.setMaximumSize(Size.INPUT_PANEL_PANEL);

		passengerCarPanel.add(passengerCarPanelButton());
		passengerCarPanel.add(passengerCarPanelFields());

		return passengerCarPanel;
	}

	private JPanel passengerCarPanelButton() {
		JPanel passengerCarPanelButton = new JPanel();
		passengerCarPanelButton.setMaximumSize(Size.INPUT_PANEL_PANEL_BUTTON);

		buttonAddPassengerCar = newInputButton("Add Passenger Car");
		passengerCarPanelButton.add(buttonAddPassengerCar);

		return passengerCarPanelButton;
	}

	private JPanel passengerCarPanelFields() {
		JPanel passengerCarPanelFields = new JPanel();
		passengerCarPanelFields.setLayout(new BoxLayout(
				passengerCarPanelFields, BoxLayout.X_AXIS));
		passengerCarPanelFields.setMaximumSize(Size.INPUT_PANEL_PANEL_FIELD);

		passengerCarPanelFields.add(new JLabel("Weight: "));
		spinnerAddPassengerCarWeight = newInputSpinner(0, 0, 1000, 1);
		passengerCarPanelFields.add(spinnerAddPassengerCarWeight);

		// Space between inputs
		passengerCarPanelFields.add(Box.createRigidArea(new Dimension(10, 0)));

		passengerCarPanelFields.add(new JLabel("Capacity: "));
		spinnerAddPassengerCarCapacity = newInputSpinner(0, 0, 300, 1);
		passengerCarPanelFields.add(spinnerAddPassengerCarCapacity);

		return passengerCarPanelFields;
	}

	private JPanel freightCarPanel() {
		JPanel freightCarPanel = new JPanel();
		freightCarPanel.setLayout(new BoxLayout(freightCarPanel,
				BoxLayout.Y_AXIS));
		freightCarPanel.setMaximumSize(Size.INPUT_PANEL_PANEL);

		freightCarPanel.add(freightCarPanelButton());
		freightCarPanel.add(freightCarPanelFields());

		return freightCarPanel;
	}

	private JPanel freightCarPanelButton() {
		JPanel freightCarPanelButton = new JPanel();
		freightCarPanelButton.setMaximumSize(Size.INPUT_PANEL_PANEL_BUTTON);

		buttonAddFreightCar = newInputButton("Add Freight Car");
		freightCarPanelButton.add(buttonAddFreightCar);

		return freightCarPanelButton;
	}

	private JPanel freightCarPanelFields() {
		JPanel freightCarPanelFields = new JPanel();
		freightCarPanelFields.setLayout(new BoxLayout(freightCarPanelFields,
				BoxLayout.X_AXIS));
		freightCarPanelFields.setMaximumSize(Size.INPUT_PANEL_PANEL_FIELD);

		freightCarPanelFields.add(new JLabel("Weight"));
		spinnerAddFreightCarWeight = newInputSpinner(0, 0, 1000, 1);
		freightCarPanelFields.add(spinnerAddFreightCarWeight);

		// Space between inputs
		freightCarPanelFields.add(Box.createRigidArea(new Dimension(10, 0)));

		freightCarPanelFields.add(new JLabel("Type: "));
		String[] types = { "D", "G", "R" };
		comboboxAddFreightCarType = new JComboBox(types);
		comboboxAddFreightCarType.setSelectedIndex(0);
		freightCarPanelFields.add(comboboxAddFreightCarType);

		return freightCarPanelFields;
	}

	private JPanel removeCarPanel() {
		JPanel removeCarPanel = new JPanel();
		removeCarPanel
				.setLayout(new BoxLayout(removeCarPanel, BoxLayout.Y_AXIS));
		removeCarPanel.setMaximumSize(Size.INPUT_PANEL_PANEL);

		buttonRemoveCar = newInputButton("Remove Carriage");
		buttonRemoveAllCars = newInputButton("Remove All Carriages");

		removeCarPanel.add(buttonRemoveCar);
		removeCarPanel.add(buttonRemoveAllCars);

		return removeCarPanel;
	}

	private JPanel passengersPanel() {
		JPanel passengersPanel = new JPanel();
		passengersPanel.setLayout(new BoxLayout(passengersPanel,
				BoxLayout.Y_AXIS));
		passengersPanel.setMaximumSize(Size.INPUT_PANEL_PANEL);

		passengersPanel.add(passengersPanelButton());
		passengersPanel.add(passengersPanelFields());

		return passengersPanel;
	}

	private JPanel passengersPanelButton() {
		JPanel passengersPanelButton = new JPanel();
		passengersPanelButton.setMaximumSize(Size.INPUT_PANEL_PANEL_BUTTON);

		buttonAddPassengers = newInputButton("Add Passengers");
		passengersPanelButton.add(buttonAddPassengers);

		return passengersPanelButton;
	}

	private JPanel passengersPanelFields() {
		JPanel passengersPanelFields = new JPanel();
		passengersPanelFields.setLayout(new BoxLayout(passengersPanelFields,
				BoxLayout.X_AXIS));
		passengersPanelFields.setMaximumSize(Size.INPUT_PANEL_PANEL_FIELD);

		passengersPanelFields.add(new JLabel("Number: "));
		spinnerAddPassengers = newInputSpinner(0, 0, 500, 1);
		passengersPanelFields.add(spinnerAddPassengers);

		return passengersPanelFields;
	}

	private JButton newInputButton(String text) {
		JButton newButton = new JButton(text);
		newButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		newButton.setMaximumSize(Size.INPUT_PANEL_PANEL_BUTTON);
		return newButton;
	}

	private JSpinner newInputSpinner(int start, int min, int max, int step) {
		SpinnerModel spinnerModel = new SpinnerNumberModel(start, min, max,
				step);
		JSpinner spinner = new JSpinner(spinnerModel);
		return spinner;
	}

	private JPanel loadPanel() {

		/*
		 * loadPanel displays information about load. It is further divided into
		 * two panels to display the coloured header status in one, and the
		 * details in the other
		 */
		JPanel loadPanel = new JPanel();
		loadPanel.setLayout(new BoxLayout(loadPanel, BoxLayout.Y_AXIS));
		loadPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		loadPanel.setMaximumSize(Size.LOAD_AND_SEATING_PANELS);

		loadPanel.add(loadPanelTop());
		loadPanel.add(Box.createRigidArea(new Dimension(0, 20)));
		loadPanel.add(loadPanelBottom());
		loadPanel.add(Box.createVerticalGlue());

		return loadPanel;
	}

	private JPanel loadPanelTop() {

		/*
		 * loadPanelTop displays the coloured header
		 */
		JPanel loadPanelTop = new JPanel();

		labelLoadCanMove = new JLabel("Can Move");
		labelLoadCanMove.setFont(new Font("Serif", Font.PLAIN, 36));
		labelLoadCanMove.setForeground(Color.GREEN);
		loadPanelTop.add(labelLoadCanMove);

		return loadPanelTop;
	}

	private JPanel loadPanelBottom() {

		/*
		 * loadPanelBottom is further divided into two panels. Left, to display
		 * the static description labels, and right, to display the dynamic
		 * data.
		 */
		JPanel loadPanelBottom = new JPanel();
		loadPanelBottom.setLayout(new BoxLayout(loadPanelBottom,
				BoxLayout.X_AXIS));
		
		loadPanelBottom.add(loadPanelBottomLeft());
		loadPanelBottom.add(Box.createHorizontalGlue());
		loadPanelBottom.add(loadPanelBottomRight());
		
		return loadPanelBottom;
	}

	private JPanel loadPanelBottomLeft() {
		JPanel loadPanelBottomLeft = new JPanel();		
		loadPanelBottomLeft.setLayout(new BoxLayout(loadPanelBottomLeft,
				BoxLayout.Y_AXIS));
		loadPanelBottomLeft.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		loadPanelBottomLeft.add(newStatusLabel("Load Capacity:"));
		loadPanelBottomLeft.add(newStatusLabel("Current Load:"));
		loadPanelBottomLeft.add(newStatusLabel("Remaining:"));
		
		return loadPanelBottomLeft;
	}

	private JPanel loadPanelBottomRight() {
		JPanel loadPanelBottomRight = new JPanel();		
		loadPanelBottomRight.setLayout(new BoxLayout(loadPanelBottomRight,
				BoxLayout.Y_AXIS));
		loadPanelBottomRight.setAlignmentX(Component.RIGHT_ALIGNMENT);
		
		labelLoadCapacity = newStatusLabel("0");
		loadPanelBottomRight.add(labelLoadCapacity);
		
		labelLoadCurrent = newStatusLabel("1");
		loadPanelBottomRight.add(labelLoadCurrent);
		
		labelLoadRemaining = newStatusLabel("2");
		loadPanelBottomRight.add(labelLoadRemaining);
		
		return loadPanelBottomRight;
	}

	private JPanel seatingPanel() {
		/*
		 * seatingPanel displays information about seating. It is further divided into
		 * two panels to display the coloured header status in one, and the
		 * details in the other
		 */
		JPanel seatingPanel = new JPanel();
		seatingPanel.setLayout(new BoxLayout(seatingPanel, BoxLayout.Y_AXIS));
		seatingPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		seatingPanel.setMaximumSize(Size.LOAD_AND_SEATING_PANELS);

		seatingPanel.add(seatingPanelTop());
		seatingPanel.add(Box.createRigidArea(new Dimension(0, 20)));
		seatingPanel.add(seatingPanelBottom());
		seatingPanel.add(Box.createVerticalGlue());
		
		return seatingPanel;
	}
	
	private JPanel seatingPanelTop() {

		/*
		 * seatingPanelTop displays the coloured header
		 */
		JPanel seatingPanelTop = new JPanel();

		labelSeatingAvailable = new JLabel("Seats Available");
		labelSeatingAvailable.setFont(new Font("Serif", Font.PLAIN, 36));
		labelSeatingAvailable.setForeground(Color.GREEN);
		seatingPanelTop.add(labelSeatingAvailable);

		return seatingPanelTop;
	}
	
	private JPanel seatingPanelBottom() {

		/*
		 * seatingPanelBottom is further divided into two panels. Left, to display
		 * the static description labels, and right, to display the dynamic
		 * data.
		 */
		JPanel seatingPanelBottom = new JPanel();
		seatingPanelBottom.setLayout(new BoxLayout(seatingPanelBottom,
				BoxLayout.X_AXIS));
		
		seatingPanelBottom.add(seatingPanelBottomLeft());
		seatingPanelBottom.add(Box.createHorizontalGlue());
		seatingPanelBottom.add(seatingPanelBottomRight());
		
		return seatingPanelBottom;
	}
	
	private JPanel seatingPanelBottomLeft() {
		JPanel seatingPanelBottomLeft = new JPanel();		
		seatingPanelBottomLeft.setLayout(new BoxLayout(seatingPanelBottomLeft,
				BoxLayout.Y_AXIS));
		seatingPanelBottomLeft.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		seatingPanelBottomLeft.add(newStatusLabel("Seating Capacity:"));
		seatingPanelBottomLeft.add(newStatusLabel("Current Seating:"));
		seatingPanelBottomLeft.add(newStatusLabel("Remaining:"));
		seatingPanelBottomLeft.add(newStatusLabel("Passengers Left Over:"));
		
		return seatingPanelBottomLeft;
	}
	
	private JPanel seatingPanelBottomRight() {
		JPanel seatingPanelBottomRight = new JPanel();		
		seatingPanelBottomRight.setLayout(new BoxLayout(seatingPanelBottomRight,
				BoxLayout.Y_AXIS));
		seatingPanelBottomRight.setAlignmentX(Component.RIGHT_ALIGNMENT);
		
		labelSeatingCapacity = newStatusLabel("0");
		seatingPanelBottomRight.add(labelSeatingCapacity);
		
		labelSeatingCurrent = newStatusLabel("1");
		seatingPanelBottomRight.add(labelSeatingCurrent);
		
		labelSeatingRemaining = newStatusLabel("2");
		seatingPanelBottomRight.add(labelSeatingRemaining);
		
		labelPassengersLeftOver = newStatusLabel("3");
		seatingPanelBottomRight.add(labelPassengersLeftOver);
		
		return seatingPanelBottomRight;
	}
	
	private JLabel newStatusLabel(String text) {
		JLabel newStatusLabel = new JLabel(text);
		newStatusLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
		return newStatusLabel;
	}

	/**
	 * GUI components are passed an action listener that detects and handles
	 * events.
	 * 
	 * @param actionListener
	 *            The listener that will handle the events.
	 */
	public void addActionListener(ActionListener actionListener) {
		// TODO Auto-generated method stub

	}
	
	public void clearCarriages() {
		pnlDisplayCarriages.removeAll();
		pnlDisplayCarriages.invalidate();
		pnlDisplayCarriages.validate();
	}
	
	public void addCarriage(JPanel carPanel) {
		System.out.println("test");
		pnlDisplayCarriages.add(Box.createRigidArea(new Dimension(0, 30)));
		
		pnlDisplayCarriages.add(carPanel);
		
		pnlDisplayCarriages.revalidate();
		// insert rigidarea
	}
	
	public void clearStatus() {
		pnlDisplayStatus.removeAll();
		pnlDisplayStatus.revalidate();
	}

	/*
	 * Button getters
	 */
	public JButton getButtonAddLocomotive() {
		return buttonAddLocomotive;
	}

	public JButton getButtonAddPassengerCar() {
		return buttonAddPassengerCar;
	}

	public JButton getButtonAddFreightCar() {
		return buttonAddFreightCar;
	}

	public JButton getButtonRemoveCar() {
		return buttonRemoveCar;
	}

	public JButton getButtonRemoveAllCars() {
		return buttonRemoveAllCars;
	}

	public JButton getButtonAddPassengers() {
		return buttonAddPassengers;
	}
	
	/*
	 * Spinner getters
	 */
	public JSpinner getSpinnerAddLocomotiveWeight() {
		return spinnerAddLocomotiveWeight;
	}
	
	public JSpinner getSpinnerAddPassengerCarWeight() {
		return spinnerAddPassengerCarWeight;
	}
	
	public JSpinner getSpinnerAddPassengerCarCapacity() {
		return spinnerAddPassengerCarCapacity;
	}
	
	public JSpinner getSpinnerAddFreightCarWeight() {
		return spinnerAddFreightCarWeight;
	}
	
	public JSpinner getSpinnerAddPassengers() {
		return spinnerAddPassengers;
	}
	
	/*
	 * ComboBox getters
	 */
	public JComboBox getComboboxAddLocomotiveClassNumber() {
		return comboboxAddLocomotiveClassNumber;
	}
	
	public JComboBox getComboboxAddLocomotiveClassLetter() {
		return comboboxAddLocomotiveClassLetter;
	}
	
	public JComboBox getComboboxAddFreightCarType() {
		return comboboxAddFreightCarType;
	}
	
	/*
	 * Label getters
	 */
	public JLabel getLabelLoadCapacity() {
		return labelLoadCapacity;
	}
	
	public JLabel getLabelLoadCurrent() {
		return labelLoadCurrent;
	}
	
	public JLabel getLabelLoadRemaining() {
		return labelLoadRemaining;
	}
	
	public JLabel getLabelLoadCanMove() {
		return labelLoadCanMove;
	}
	
	public JLabel getLabelSeatingCapacity() {
		return labelSeatingCapacity;
	}
	
	public JLabel getLabelSeatingCurrent() {
		return labelSeatingCurrent;
	}
	
	public JLabel getLabelSeatingRemaining() {
		return labelSeatingRemaining;
	}
	
	public JLabel getLabelPassengersLeftOver() {
		return labelPassengersLeftOver;
	}
	
	public JLabel getLabelSeatingAvailable() {
		return labelSeatingAvailable;
	}
}
