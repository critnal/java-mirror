/**
 * @author	Alexander Crichton n6878296
 * @author Kerry England 
 * @version	1.0
 * @since	23/05/2013
 */
package asgn2GUI;

import javax.swing.JFrame;

import asgn2Train.DepartingTrain;

public class TrainMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				DepartingTrain train = new DepartingTrain();
		
				TrainGUI gui = new TrainGUI();
		
				TrainController controller = new TrainController(train, gui);
		
				gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				gui.pack();
				gui.setVisible(true);
			}
		});
	}

}
