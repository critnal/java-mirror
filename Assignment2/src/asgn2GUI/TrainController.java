/**
 * @author	Alexander Crichton n6878296
 * @author Kerry England 
 * @version	1.0
 * @since	23/05/2013
 */
package asgn2GUI;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import asgn2Exceptions.TrainException;
import asgn2RollingStock.Locomotive;
import asgn2RollingStock.PassengerCar;
import asgn2RollingStock.RollingStock;
import asgn2Train.DepartingTrain;

public class TrainController {

	// The model
	private DepartingTrain train;

	// The view
	private TrainGUI gui;

	public TrainController(DepartingTrain train, TrainGUI gui) {
		this.train = train;
		this.gui = gui;
		addListeners();
	}
	
	/**
	 * This class handles GUI events. It gets the source of the event and
	 * performs some action based on that source. An instance of this class gets
	 * passed to the GUI in the TrainController constructor.
	 */
	private class TrainListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			Component source = (Component)e.getSource();			
//			if (source == gui.getAddLocomotiveButton()) {
//				int weight = gui.getAddLocomotiveWeightField();
//				String class = gui.getAddLocomotiveClassField();
//				Locomotive locomotive = new Locomotive(weight, class);
//				train.addCarriage(locomotive);
//			}
			
			if (source == gui.getButtonAddLocomotive()) {
				addLocomotiveToTrain();
			} else if (source == gui.getButtonAddPassengerCar()) {
				addPassengerCarToTrain();
			} else if (source == gui.getButtonRemoveCar()) {
				try {
					train.removeCarriage();
				} catch (TrainException e1) {
					e1.printStackTrace();
				}				
			}
			updateDisplay();
		}
	}
	
	private void addListeners() {
		gui.getButtonAddLocomotive().addActionListener(new TrainListener());
		gui.getButtonAddPassengerCar().addActionListener(new TrainListener());
		gui.getButtonRemoveCar().addActionListener(new TrainListener());
		gui.getButtonRemoveAllCars().addActionListener(new TrainListener());
		gui.getButtonAddPassengers().addActionListener(new TrainListener());
	}
	
	private void addLocomotiveToTrain() {
		try {
			Integer weight = (Integer)gui.getSpinnerAddLocomotiveWeight().getValue();
			String class1 = (String)gui.getComboboxAddLocomotiveClassNumber().getSelectedItem();
			String class2 = (String)gui.getComboboxAddLocomotiveClassLetter().getSelectedItem();
			String classification = "" + class1 + class2;
			train.addCarriage(new Locomotive(weight, classification));
		} catch (TrainException e) {
			e.printStackTrace();
		}
	}
	
	private void addPassengerCarToTrain() {
		try {
			train.addCarriage(new PassengerCar(1, 1));
		} catch (TrainException e) {
			e.printStackTrace();
		}
	}
	
	private void updateDisplay() {		
		updateStatusDisplay();
		updateCarriageDisplay();
	}
	
	private void updateCarriageDisplay() {
		gui.clearCarriages();
		
		RollingStock currentCarriage = train.firstCarriage();
		if (currentCarriage != null) {
			addLocomotiveToDisplay(currentCarriage);
			
			while ((currentCarriage = train.nextCarriage()) != null) {
				if (currentCarriage instanceof PassengerCar) {
					addLocomotiveToDisplay(currentCarriage);
				}
			}
		}
	}
	
	private void addLocomotiveToDisplay(RollingStock carriage) {
		JPanel carPanel = new JPanel();
		carPanel.setLayout(new BoxLayout(carPanel, BoxLayout.Y_AXIS));
		carPanel.setMinimumSize(Size.CAR_PANEL);
		carPanel.setMaximumSize(Size.CAR_PANEL);
		carPanel.setBorder(BorderFactory.createLineBorder(Color.gray, 2));
		
		
		
		gui.addCarriage(carPanel);
	}
	
	private void updateStatusDisplay() {
		gui.clearStatus();
		
//		updateSeatingDisplay();
	}

	private void updateLoadDisplay() {
		gui.getLabelLoadCurrent().setText(getLoadCurrent());
		
		Container parent = gui.getLabelLoadCurrent();
		parent.remove(gui.getLabelLoadCurrent());
		parent.validate();
		parent.repaint();
	}
	
	private String getLoadCurrent() {
		Integer load = 2;
		
		return load.toString();
	}
	

}
